import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { AsiaEffects } from './asia.effects';

describe('CountryEffects', () => {
  let actions$: Observable<any>;
  let effects: AsiaEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AsiaEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(AsiaEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
