import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { EuropeEffects } from './europe.effects';

describe('EuropeEffects', () => {
  let actions$: Observable<any>;
  let effects: EuropeEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        EuropeEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(EuropeEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
