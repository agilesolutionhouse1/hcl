import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationContainerPageComponent } from './location-container-page.component';
import { Store, StoreModule } from '@ngrx/store';

describe('LocationContainerPageComponent', () => {
  let component: LocationContainerPageComponent;
  let fixture: ComponentFixture<LocationContainerPageComponent>;
  let store: Store;

  beforeEach(async() => {
    TestBed.configureTestingModule({
      imports: [ StoreModule.forRoot({}) ],
      declarations: [ LocationContainerPageComponent ]
    });

    await TestBed.compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationContainerPageComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(Store);

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
