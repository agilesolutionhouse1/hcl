import { state } from '@angular/animations';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromReducer from '../reducers/reducer.reducer';

export const currentStateSelector =  (state: fromReducer.AppState) => {
    return state as fromReducer.AppState;
}

export const getRegiontSelector = createSelector (
    currentStateSelector,
    (state: fromReducer.AppState) => state['count'].regions
);
