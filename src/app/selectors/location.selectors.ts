import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppState } from '../reducers/reducer.reducer';

export const currentStateSelector = (state: AppState) => state;

export const getLocationResultSelector = createSelector (
    currentStateSelector,
    (state: AppState) => state['count'].selectLocationResult
)

export const getCurrentRegionSelector = createSelector (
    currentStateSelector,
    (state: AppState) => state['count'].defaultRegion
)
