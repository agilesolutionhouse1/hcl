import { TestBed } from '@angular/core/testing';

import { AsiaService } from './asia.service';

describe('AsiaService', () => {
  let service: AsiaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AsiaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
