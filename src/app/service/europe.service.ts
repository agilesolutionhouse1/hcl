import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { serviceUrl } from 'src/app/config';
@Injectable()
export class EuropeService {
  constructor(private httpClient: HttpClient) { }

  public getLocations() : Observable<Location[]> {
    // const header = this.getHeader();
    return this.httpClient.get<Location[]>(serviceUrl + 'region/europe');
  }

  // private getHeader() {
  //   return {
  //     headers: new HttpHeaders({
  //       'Content-Type':  'application/json',
  //       'Access-Control-Allow-Origin':'*',
  //       'Accept':'application/json, text/plain, */*',
  //       'Cache-Control':  'no-cache, no-store, must-revalidate, post- check=0, pre-check=0',
  //       'Pragma': 'no-cache',
  //       'Expires': '0',
  //     }),
  //     observe: 'response'
  //   };
  // }
}
